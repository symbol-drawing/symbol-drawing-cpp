//Renan Câmara Pereira 2022

#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <tuple>
#include <omp.h>
#include <algorithm>
#include "symboldrawing.h"

using namespace std;


int main(void)
{	

	//pixelFigure fig1(21,21);	

	//fig1.addSquare(10, 10, 21, 1);
	//fig1.addSquare(10, 10, 19, 0);

	//fig1.addLine(1, 1, 6, 6, 1);
	//ig1.addLine(0, 20, 19, 2, 1);
	//fig1.addLine(9, 2, 9, 9, 1);

/*
	fig1.addSquare(10, 10, 21, 1);
	fig1.addSquare(10, 10, 19, 0);
	fig1.addSquare(10, 10, 17, 1);
	fig1.addSquare(10, 10, 15, 0);
	fig1.addSquare(10, 10, 13, 1);
	fig1.addSquare(10, 10, 11, 0);
	fig1.addSquare(10, 10, 9, 1);
	fig1.addSquare(10, 10, 7, 0);
	fig1.addSquare(10, 10, 5, 1);
	fig1.addSquare(10, 10, 3, 0);
	fig1.addSquare(10, 10, 1, 1);
*/

/*
	int figW = 50;
	int figH = 50;
	pixelFigure fig1(figW, figH);	

	int skullCenterX = 25;
	int skullCenterY = 25;
	int skullRadius = 50;
	//fig1.addSquare("c", 25, 25, 50, 1);
	//fig1.addSquare("c", 25, 25, 48, 0);

	fig1.addCircle("c", 23, 28, 14, 1);
	fig1.addSquare("c", 23, 9, 11, 1);

	fig1.addCircle("c", 17, 29, 4, 0);
	//fig1.addCircle("c", 17, 29, 1, 1);
	fig1.addCircle("c", 29, 29, 4, 0);
	//fig1.addCircle("c", 29, 29, 1, 1);

	fig1.draw(false);

	fig1.drawToFile(false, "teste.dat");
	fig1.drawToTexTabular(false, "fig1.tex");
*/


/*
	pixelFigure fig2(50, 50);	

	int skullR = 15;
	int skullCenterX = 23;
	int skullCenterY = 32;
	int jawLength = skullR;
	fig2.addCircle("c", skullCenterX, skullCenterY, skullR, 1);
	int d = skullR;
	fig2.addSquare("c", skullCenterX, skullCenterY-(skullR+jawLength/2-1), jawLength, 1);

	fig2.addCircle("c", skullCenterX*(1-6./23), skullCenterY, skullR*4./15, 0);
	fig2.addCircle("c", skullCenterX*(1+6./23), skullCenterY, skullR*4./15, 0);


	fig2.draw(false);
*/

	pixelFigure fig2(50, 50);

	fig2.addCircle("d", 1, 8, 8, 1);


	fig2.draw(false);

	return 0;
}



