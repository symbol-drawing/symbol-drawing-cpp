#ifndef SYMBOLDRAWING_H
#define SYMBOLDRAWING_H

using namespace std;


class pixelFigure
{
private:
	vector< vector<int> > figure;
	int width = 0;
	int height = 0;

public:
	void setDimensions(int , int );
	pixelFigure(int, int);
	void draw(bool);
	void drawToFile(bool , string );
	void drawToTexTabular(bool , string );
	void setPixel(int , int , int );
	void addCircle(string, int , int , int , int );
	void addSquare(string, int , int , int , int );
	void addTriangleRect(int , int , int , int );
	void addLine(int , int , int , int , int );
};



#endif