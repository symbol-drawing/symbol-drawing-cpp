#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <tuple>
#include <omp.h>
#include <algorithm>
#include "symboldrawing.h"

using namespace std;


void pixelFigure::setDimensions(int w, int h)
{	
	width = w;
	height = h;
}

pixelFigure::pixelFigure(int W, int H)
{	
	setDimensions(W, H);
	vector<int> aux(width,0);
	for (int x = 0; x < height; ++x)
	{	
		figure.push_back( aux );
	}
}

void pixelFigure::draw(bool withBackground)
{	
	for (int y = 0; y < figure.size(); ++y)
	{
		for (int x = 0; x < figure[y].size(); ++x)
		{	
			if (figure[y][x]==0)
			{	
				if (withBackground==false){ cout << " " << "  "; }
				else{ cout << figure[y][x] << "  "; }
			}
			else
			{
				cout << figure[y][x] << "  ";
			}
		}
		cout << "\n";
	}
}

void pixelFigure::drawToFile(bool withBackground, string filename)
{	
	//Create file
	std::ofstream file;
    file.open(filename, std::ofstream::out | std::ios::trunc);

	for (int y = 0; y < figure.size(); ++y)
	{
		for (int x = 0; x < figure[y].size(); ++x)
		{	
			if (figure[y][x]==0)
			{	
				if (withBackground==false){ file << " " << "  "; }
				else{ file << figure[y][x] << "  "; }
			}
			else
			{
				file << figure[y][x] << "  ";
			}
		}
		file << "\n";
	}

	file.close();
}

void pixelFigure::drawToTexTabular(bool withBackground, string filename)
{	
	//Create file
	std::ofstream file;
    file.open(filename, std::ofstream::out | std::ios::trunc);

    string Ncol = "c";
    for (int i = 0; i < width; ++i){ Ncol = Ncol + "c"; }

	file << "\\begin{tabular}{" << Ncol << "}" << "\n";

	for (int y = 0; y < figure.size(); ++y)
	{
		for (int x = 0; x < figure[y].size(); ++x)
		{	
			if (figure[y][x]==0)
			{	
				if (withBackground==false){ file << " "; }
				else{ file << figure[y][x]; }

				if( x < (figure[y].size()-1) ){ file << " & "; }
			}
			else
			{
				file << figure[y][x];
				if( x < (figure[y].size()-1) ){ file << " & "; }
			}
		}
		file << "\\\\" << "\n";
	}


	file << "\\end{tabular}" << "\n";


	file.close();
}


void pixelFigure::setPixel(int x, int y, int value)
{	
	if (x>=0 && x<width)
	{
		if (y>=0 && y<height)
		{
			figure[abs((height-1)-y)][x] = value;
		}
	}
}

void pixelFigure::addCircle(string relPos, int xPos, int yPos, int radius, int value)
{	
	double xCenter;
	double yCenter;
	if ( relPos=="c" )
	{	
		xCenter = xPos;
		yCenter = yPos;
	}
	else if( relPos=="d" )
	{	
		xCenter = xPos+radius;
		yCenter = yPos+radius;
	}
	else{ cout << "Relative position to define circle not defined." << "\n"; abort(); }

	for (int y = 0; y < figure.size(); ++y)
	{
		for (int x = 0; x < figure[y].size(); ++x)
		{	
			if ( (pow(x-xCenter,2) + pow(y-yCenter,2))<=pow(radius,2) )
			{
				setPixel(x, y, value);
			}
		}
	}

}

void pixelFigure::addSquare(string relPos, int xPos, int yPos, int side, int value)
{	
	double xCenter;
	double yCenter;
	if ( relPos=="c" )
	{	
		xCenter = xPos;
		yCenter = yPos;
	}
	else if( relPos=="d" )
	{
		xCenter = xPos + side/2;
		yCenter = yPos + side/2;
	}
	else{ cout << "Relative position to define square not defined." << "\n"; abort(); }

	for (int y = 0; y < figure.size(); ++y)
	{
		for (int x = 0; x < figure[y].size(); ++x)
		{	
			if ( 2*x>=2*xCenter-side && 2*x<2*xCenter+side && 2*y>=2*yCenter-side && 2*y<2*yCenter+side ){ setPixel(x, y, value); }
		}
	}
}

void pixelFigure::addTriangleRect(int xCenter, int yCenter, int side, int value)
{	
	for (int y = 0; y < figure.size(); ++y)
	{
		for (int x = 0; x < figure[y].size(); ++x)
		{	
			if ( 2*x>=2*xCenter-side && 2*x<2*xCenter+side && 2*y>=2*yCenter-side && 2*y<2*yCenter+side && x>=y ){ setPixel(x, y, value); }
		}
	}
}

void pixelFigure::addLine(int x1, int y1, int x2, int y2, int value)
{	
	if( abs(y2-y1)<=abs(x2-x1) )
	{
		double m = 1.0*(y2-y1)/(x2-x1);
		int N = abs(x2-x1);
		int delta = 1*(x2-x1)/N;
		for (int i = 0; i <= N; ++i)
		{	
			int x = x1 + i*delta;
			int y = round( m*x + (y1-m*x1) );
			setPixel(x, y, value);
		}
	}
	else
	{
		double m = 1.0*(x2-x1)/(y2-y1);
		int N = abs(y2-y1);
		int delta = 1*(y2-y1)/N;
		for (int i = 0; i <= N; ++i)
		{	
			int y = y1 + i*delta;
			int x = round( m*y + (x1-m*y1) );
			setPixel(x, y, value);
		}
	}
}

